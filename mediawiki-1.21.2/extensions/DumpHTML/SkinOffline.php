<?php

/**
 * Default skin for HTML dumps, based on MonoBook.php
 */

if( !defined( 'MEDIAWIKI' ) )
	die( 1 );

/**
 * Inherit main code from SkinTemplate, set the CSS and template filter.
 * @todo document
 * @ingroup Skins
 */
class SkinOffline extends SkinTemplate {
	/** Using monobook. */
	var $template  = 'SkinOfflineTemplate';

	function setupTemplate( $className, $repository = false, $cache_dir = false ) {
		global $wgFavicon, $wgStylePath;
		$tpl = parent::setupTemplate( $className, $repository, $cache_dir );
		$tpl->set( 'skinpath', "$wgStylePath/offline" );
		$tpl->set( 'favicon', $wgFavicon );
		return $tpl;
	}

	function buildSidebar() {
		$sections = parent::buildSidebar();
		$badMessages = array( 'recentchanges-url', 'randompage-url' );
		$badUrls = array();
		foreach ( $badMessages as $msg ) {
			$badUrls[] = self::makeInternalOrExternalUrl( wfMsgForContent( $msg ) );
		}
		foreach ( $sections as $heading => $section ) {
			if (!is_array($section)) {
				// A raw HTML chunk, such as provided by Collection ext.
				// Just ignore these so they don't explode.
				unset( $sections[$heading] );
				continue;
			}
			foreach ( $section as $index => $link ) {
				if ( in_array( $link['href'], $badUrls ) ) {
					unset( $sections[$heading][$index] );
				}
			}
		}
		return $sections;
	}

	function buildContentActionUrls( $content_navigation ) {
		global $wgHTMLDump;

		$content_actions = array();
		$nskey = $this->getNameSpaceKey();
		$content_actions[$nskey] = $this->tabAction(
			$this->getTitle()->getSubjectPage(),
			$nskey,
			!$this->getTitle()->isTalkPage() );

		if( $this->getTitle()->canTalk() ) {
			$content_actions['talk'] = $this->tabAction(
				$this->getTitle()->getTalkPage(),
				'talk',
				$this->getTitle()->isTalkPage(),
				'',
				true);
		}

		if ( isset( $wgHTMLDump ) ) {
			$content_actions['current'] = array(
				'text' => wfMsg( 'currentrev' ),
				'href' => str_replace( '$1', wfUrlencode( $this->getTitle()->getPrefixedDBkey() ),
					$wgHTMLDump->oldArticlePath ),
				'class' => false
			);
		}
		return $content_actions;
	}

	function makeBrokenLinkObj( &$nt, $text = '', $query = '', $trail = '', $prefix = '' ) {
		if ( !isset( $nt ) ) {
			return "<!-- ERROR -->{$prefix}{$text}{$trail}";
		}

		if ( $nt->getNamespace() == NS_CATEGORY ) {
			# Determine if the category has any articles in it
			$dbr = wfGetDB( DB_SLAVE );
			$hasMembers = $dbr->selectField( 'categorylinks', '1', 
				array( 'cl_to' => $nt->getDBkey() ), __METHOD__ );
			if ( $hasMembers ) {
				return $this->makeKnownLinkObj( $nt, $text, $query, $trail, $prefix );
			}
		}

		if ( $text == '' ) {
			$text = $nt->getPrefixedText();
		}
		return $prefix . $text . $trail;
	}

	function printSource() {
		return '';
	}
}

/**
 * @todo document
 * @ingroup Skins
 */
class SkinOfflineTemplate extends QuickTemplate {
	/**
	 * Template filter callback for MonoBook skin.
	 * Takes an associative array of data set from a SkinTemplate-based
	 * class, and a wrapper for MediaWiki's localization database, and
	 * outputs a formatted page.
	 *
	 * @private
	 */
    function execute() {
        wfSuppressWarnings();

?><h1 class="firstHeading"><?php $this->data['displaytitle']!=""?$this->html('title'):$this->text('title') ?></h1>
<div id="bodyContent">
<h3 id="siteSub"><?php $this->msg('tagline') ?></h3>
<div id="contentSub"><?php $this->html('subtitle') ?></div>
<?php $this->html('bodytext') ?>
<?php if($this->data['catlinks']) { ?><?php  $this->html('catlinks') ?><?php
     }
     if( $this->data['language_urls'] ) { ?>
     <h5><?php $this->msg('otherlanguages') ?></h5>
     <div class="pBody">
     <ul>
     <?php foreach($this->data['language_urls'] as $langlink) { ?>
     <li>
     <a href="<?php echo htmlspecialchars($langlink['href']) ?>"><?php echo $langlink['text'] ?></a>
     </li>
     <?php } ?>
     </ul>
     </div>
     <?php } ?>
     <div class="visualClear"></div> </div>
<?php
		wfRestoreWarnings();
	}
}
?>
