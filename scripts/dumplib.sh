#!/bin/bash

getSourceDumps()
{
    LANG="$1"
    echo "Downloading dumps for language $LANG."
    DATE="$2"
    [ e"$DATE" = e ] && DATE="latest"

    if [[ ! "$LANG" =~ ^.*(wiktionary|wiki(|quote|books|voyage))$ ]]
    then
        LANG="$LANG"wiki
    fi

    (cd "$SOURCEDUMPDIR/"
        for suffix in pages-articles.xml.bz2 image.sql.gz category.sql.gz categorylinks.sql.gz
        do
            wget --progress=dot:mega -c "http://download.wikimedia.org/""$LANG""/""$DATE""/""$LANG""-""$DATE"-"$suffix" && \
            ln -sf "$LANG"-"$DATE"-"$suffix" wiki-latest-"$suffix" && \
            continue
            return 1
        done
    )
    return 0
}

getCommonsImageSourceDumps()
{
    LANG="commons"
    echo "Downloading dumps for language commons."
    DATE="$1"
    [ e"$DATE" = e ] && DATE="latest"

    DESTDIR="$SOURCEDUMPDIR/"
    (cd "$SOURCEDUMPDIR/"
        wget --progress=dot:mega -c "http://download.wikimedia.org/""$LANG"wiki"/""$DATE""/""$LANG"wiki"-""$DATE"-image.sql.gz && \
        ln -sf "$LANG"wiki-"$DATE"-image.sql.gz wiki-latest-image.sql.gz
    ) && return 0
    return 1
}

discardDatabase()
{
    killall -9 mysqld > /dev/null 2>&1
    echo "Removing database files..."
    rm -r "$MYSQLDIR/data/*" >/dev/null 2>&1
    return 0
}

shutdownDatabase()
{
    mysqladmin $MYSQL_OPTS shutdown
}

checkMysqlRunning()
{
    mysql $MYSQL_OPTS < /dev/null 2>/dev/null || "$SCRIPTDIR/startmysql.sh"
}


reCreateTables()
{
    PREFIX="$1"
    echo "Recreating db tables for prefix \"$PREFIX""\"."
    (
    grep "CREATE TABLE" "$MEDIAWIKIDIR"/maintenance/tables.sql | \
        sed -e 's/CREATE TABLE/DROP TABLE IF EXISTS/' | \
        sed -e 's/ ($/;/' | \
        sed -e 's/\/\*_\*\//'"$PREFIX"'/';
    cat "$MEDIAWIKIDIR/maintenance/tables.sql" | \
        sed -e 's/\/\*_\*\//'"$PREFIX"'/' | \
        sed -e 's/\/\*i\*\//'"$PREFIX"'/' | \
        sed -e 's/\/\*$wgDBTableOptions\*\//ENGINE=InnoDB, DEFAULT CHARSET=binary/';
    cat "$MEDIAWIKIDIR/maintenance/wikipedia-interwiki.sql" | \
        sed -e 's/\/\*_\*\//'"$PREFIX"'/' | \
        sed -e 's/\/\*i\*\//'"$PREFIX"'/' | \
        sed -e 's/\/\*$wgDBprefix\*\//'"$PREFIX"'/' | \
        sed -e 's/\/\*$wgDBTableOptions\*\//ENGINE=InnoDB, DEFAULT CHARSET=binary/';
    echo "FLUSH TABLES;"
    ) | mysql $MYSQL_OPTS -u "$dbuser" --password=$password "$database" || return 1
}

#createTablesForced()
#{
#    PREFIX="$1"
#    echo "Creating db tables for prefix \"$PREFIX""\"."
#    cat "$MEDIAWIKIDIR/maintenance/tables.sql" | \
#        sed -e 's/\/\*_\*\//'"$PREFIX"'/' | \
#        sed -e 's/\/\*i\*\//'"$PREFIX"'/' | \
#        sed -e 's/\/\*$wgDBTableOptions\*\//ENGINE=InnoDB, DEFAULT CHARSET=binary/' | \
#	grep -v 'CREATE INDEX' | \
#	grep -v 'CREATE UNIQUE INDEX' | \
#        mysql $MYSQL_OPTS -f -u "$dbuser" --password=$password "$database" >/dev/null 2>&1
#    cat "$MEDIAWIKIDIR/maintenance/wikipedia-interwiki.sql" | \
#        sed -e 's/\/\*_\*\//'"$PREFIX"'/' | \
#        sed -e 's/\/\*i\*\//'"$PREFIX"'/' | \
#        sed -e 's/\/\*$wgDBTableOptions\*\//ENGINE=InnoDB, DEFAULT CHARSET=binary/' | \
#        mysql $MYSQL_OPTS -f -u "$dbuser" --password=$password "$database" >/dev/null 2>&1
#    return 0;
#}


importLanguage()
{
    LANG="$1"
    PREFIX="$2"

    echo "Importing wikipedia $LANG"

    reCreateTables "$PREFIX" || return 1

    echo "Extracting xml dump..."
    rm "$IMPORTTEMPDIR"/* >/dev/null 2>&1

    nice bunzip2 < "$SOURCEDUMPDIR/"wiki-latest-pages-articles.xml.bz2 | \
    nice grep -v '<format>' | \
    nice grep -v '<model>' | \
    nice grep -v '<parentid>' | \
    nice grep -v '<redirect />' | \
    nice grep -v '<redirect title' | \
    nice grep -v '<sha1' | \
    nice grep -v '<ns>' | \
    nice awk -- '/<DiscussionThreading>/,/<\/DiscussionThreading>/ {next}; {print};' | \
    nice "$SCRIPTDIR"/xml2sql -o "$IMPORTTEMPDIR" -v || return 1
    # we have to remove all <redirect /> tags because xml2sql cannot handle them
    # (the bug does not occur with wikipedia dumps prior to july 2009)
    # we also have to remove <DiscussionThreading>...</DiscussionThreading>

    for x in page revision text
    do
        echo "Importing table $x..."
#        echo "FLUSH TABLES;" | mysql $MYSQL_OPTS -u "$dbuser" --password=$password "$database"
#        echo "...disabling keys"
#        DBFILE="$DATABASEDIR/$PREFIX$x"
#        nice myisamchk -r --keys-used=0 "$DBFILE" || return 1

#        echo "...importing data"
        DUMPFILE="$IMPORTTEMPDIR/$PREFIX""$x.txt"
        [ x"$PREFIX" != x ] && mv "$IMPORTTEMPDIR/$x.txt" "$DUMPFILE"
        echo "FLUSH TABLES;" | mysql $MYSQL_OPTS -u "$dbuser" --password=$password "$database"
        mysqlimport $MYSQL_OPTS -d -u "$dbuser" --password="$password" "$database" "$DUMPFILE" || return 1
        echo "FLUSH TABLES;" | mysql $MYSQL_OPTS -u "$dbuser" --password=$password "$database"

#        echo "...packing table"
#        nice myisampack "$DBFILE" || return 1
#
#        echo "...creating indexes"
#        nice myisamchk -r -q \
#            --sort_buffer_size=1024M \
#            --read_buffer=3M \
#            --write_buffer=3M \
#            --tmpdir=/tmp \
#            "$DBFILE" || return 1
#
#        echo "FLUSH TABLES;" | mysql $MYSQL_OPTS -u "$dbuser" --password=$password "$database"
        echo "done"
        rm "$DUMPFILE"
    done

    echo "Importing images..."
#    echo "...disabling keys"
#    DBFILE="$DATABASEDIR/image"
#    nice myisamchk -r --keys-used=0 "$DBFILE" || return 1

    echo "...importing data"

#    (
#    echo "FLUSH TABLES;"
#    nice gunzip -c < "$SOURCEDUMPDIR/wiki-latest-image.sql.gz" | \
#    nice awk -- '/DROP TABLE/,/-- Dumping data/ {next}; {print};'
#    # remove the drop and recreate table (with potentially different engine) statements
#    echo "FLUSH TABLES;"
#    ) | mysql $MYSQL_OPTS -u "$dbuser" --password=$password "$database" || return 1
    (
    nice gunzip -c < "$SOURCEDUMPDIR/wiki-latest-image.sql.gz" | \
       head -n 50 | \
       sed -e 's/ \(medium\|tiny\)blob / CHAR(1) /'
    echo "SET AUTOCOMMIT=0;"
    nice gunzip -c < "$SOURCEDUMPDIR/wiki-latest-image.sql.gz" | \
	tail -n +50
    echo "COMMIT;"
    ) | mysql $MYSQL_OPTS -u "$dbuser" --password=$password "$database" || return 1



    echo "Importing categories..."
    (
    nice gunzip -c < "$SOURCEDUMPDIR/wiki-latest-category.sql.gz" #| \
    #nice awk -- '/DROP TABLE/,/-- Dumping data/ {next}; {print};'
    ## remove the drop and recreate table (with potentially different engine) statements
    ) | mysql $MYSQL_OPTS -u "$dbuser" --password=$password "$database" || return 1
    (
    nice gunzip -c < "$SOURCEDUMPDIR/wiki-latest-categorylinks.sql.gz" #| \
    #nice awk -- '/DROP TABLE/,/-- Dumping data/ {next}; {print};'
    ## remove the drop and recreate table (with potentially different engine) statements
    echo "FLUSH TABLES;"
    ) | mysql $MYSQL_OPTS -u "$dbuser" --password=$password "$database" || return 1
    echo "done"

    return 0
}

importCommons()
{
    PREFIX="commons_"

    echo "Importing wikipedia commons images"

    reCreateTables "$PREFIX" || return 1

    (
    nice gunzip -c < "$SOURCEDUMPDIR/wiki-latest-image.sql.gz" | \
       head -n 50 | \
       sed -e 's/ \(medium\|tiny\)blob / CHAR(1) /'
    echo "SET AUTOCOMMIT=0;"
    nice gunzip -c < "$SOURCEDUMPDIR/wiki-latest-image.sql.gz" | \
       tail -n +50
    echo "COMMIT;"
    echo "DROP TABLE commons_image;" # XXX indexes?
    echo "RENAME TABLE image TO commons_image;"
    echo "FLUSH TABLES;"
    ) | mysql $MYSQL_OPTS -u "$dbuser" --password=$password "$database" || return 1
    echo "done"

    return 0
}

changeWikiSettings()
{
    LANG="$1"
    
    settings=
    sitename=Wikipedia
    if [[ "$LANG" =~ ^(.*)(wiktionary|wiki(|quote|books|voyage))$ ]]
    then
        REALLANG="${BASH_REMATCH[1]}"
        if [[ "$LANG" =~ ^.*wiktionary$ ]]
        then
            echo "Wiktionary!"
            sitename=Wiktionary
            settings='$wgCapitalLinks = false;'
        elif [[ "$LANG" =~ ^.*wikiquote$ ]]
        then
            sitename=Wikiquote
        elif [[ "$LANG" =~ ^.*wikibooks$ ]]
        then
            sitename=Wikibooks
        elif [[ "$LANG" =~ ^.*wikivoyage$ ]]
        then
            sitename=Wikivoyage
        fi
    else
        REALLANG="$LANG"
    fi
    WIKIUSERSETTINGS="$settings"'$wgSitename = "'"$sitename"'"; $wgDBname = "'"$database"'"; $wgDBuser = "'"$dbuser"'"; $wgDBpassword = "'"$password"'";'

    echo "Changing mediawiki settings..."
    (
    cat "$MEDIAWIKIDIR/LocalSettings.in.php" | sed -e 's/__WIKIUSERSETTINGS__/'"$WIKIUSERSETTINGS"'/'
    echo '$wgLanguageCode = "'$REALLANG'";
            $wgLocalFileRepo = array(
                "class" => "ForeignDBRepo",
                "name" => "local",
                "url" => "http://upload.wikimedia.org/wikipedia/'$REALLANG'",
                "hashLevels" => 2, // This must be the same for the other family member
                //"thumbScriptUrl" => "http://wiki.example.com/thumb.php",
                "transformVia404" => true,//!$wgGenerateThumbnailOnParse,
                "dbType" => $wgDBtype,
                "dbServer" => $wgDBserver,
                "dbUser" => $wgDBuser,
                "dbPassword" => $wgDBpassword,
                "dbName" => $wgDBname,
                "tablePrefix" => "",
                "hasSharedCache" => false,
                "descBaseUrl" => "http://'$REALLANG'.wikimedia.org/wiki/File:",
                "fetchDescription" => false
            );'
    ) > "$MEDIAWIKIDIR/LocalSettings.php" || return 1
    echo "done."
    return 0
}

multstring()
{
    string="$1"
    num="$2"
    for i in $(seq 1 "$num")
    do
        echo -n "$string"
    done
}

progressbar()
{
    index="$1"
    index=$((index + 1))
    move=$((index + 1))
    while read -r -d "\r" -n 60 data
    do
        [[ "$data" =~ 'Processing ID: '([0-9]+)' of '([0-9]+) ]] || continue
        i=${BASH_REMATCH[1]}
        max=${BASH_REMATCH[2]}
        [ $i -le 0 -o $i -gt $max ] && continue
        width=$(tput cols)
        percentage=$((i*100/max))
        fullbarlength=$((width - 30))
        barlength=$((i*fullbarlength/max))
        barrestlength=$((fullbarlength - barlength))
        if [ $percentage -le 9 ]
        then
            percentage="  $percentage"
        elif [ $percentage -le 99 ]
        then
            percentage=" $percentage"
        fi
        # move to the correct line
        text="\r"$(multstring "\n" $move)
        text="${text}($index) $percentage% ["
        text="${text}"$(multstring "#" $barlength)
        text="${text}"$(multstring "_" $barrestlength)
        text="${text}]"
        text="${text}\r\x1b[${move}F"
        echo -n -e "$text"
    done
}


dumpWiki()
{
    LANG="$1"
    SLICENUMBER="$2"
    MAXSLICES="$3"
    INDEX="$4"

    set -o pipefail
    rm -r "$DESTDUMPTEMPDIR/$SLICENUMBER" 2>/dev/null
    mkdir "$DESTDUMPTEMPDIR/$SLICENUMBER" 2>/dev/null
    nice php -d open_basedir= -d mysql.default_socket="$MYSQLDIR/mysqld.sock" "$MEDIAWIKIDIR/extensions/DumpHTML/dumpHTML.php" --interlang -d "$DESTDUMPTEMPDIR/$SLICENUMBER" --slice $SLICENUMBER/$MAXSLICES | progressbar "$INDEX"  || return 1
    return 0
}

