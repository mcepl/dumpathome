#!/bin/bash

SCRIPTDIR=$(dirname $0)
. "$SCRIPTDIR/settings.sh"

echo "(Re-)starting MySQL server...."

sed -e "s#__MYSQLDIR__#$MYSQLDIR#" < "$SCRIPTDIR/my.cnf.in" > "$MYSQLDIR/my.cnf"
chmod o-w "$MYSQLDIR/my.cnf"

killall -9 mysqld >/dev/null 2>&1

if [ ! -r "$MYSQLDIR/data/mysql/user.frm" ]
then
    "$SCRIPTDIR"/initmysql.sh || exit 1
fi

sleep 2

# evade apparmor
cp /usr/libexec/mysqld "$SCRIPTDIR" >/dev/null 2>&1

nice "$SCRIPTDIR"/mysqld $MYSQL_OPTS > /dev/null 2>&1 &

for sleeptime in 2 4 8 16 32 64
do
    sleep $sleeptime
    if mysql $MYSQL_OPTS < /dev/null 2>/dev/null
    then
        echo "CREATE DATABASE IF NOT EXISTS $database;" | mysql $MYSQL_OPTS
        echo "Restarted MySQL server."
        exit 0
    fi
done

echo "Error starting mysql server."
exit 1
