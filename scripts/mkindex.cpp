#include <iostream>
#include <fstream>
#include <vector>
#include <locale>
#include <algorithm>

using namespace std;

class TransTable {
    public:

        static inline unsigned char translate(unsigned int i) {
            static TransTable instance;
            return instance.table[i];
        }
        static inline unsigned char translate(wchar_t i) {
            return translate((unsigned int) i);
        }
    private:
        TransTable() {
            for (int i = 0; i < 0x10000; i ++)
                table[i] = '_';

            ifstream in("transtbl.dat", ios::binary | ios::in);
            while (in.good()) {
                unsigned char entry[8];
                in.read(reinterpret_cast<char *>(entry), 8);
                unsigned int key = (((unsigned int) entry[2]) << 8) |
                                     (unsigned int) entry[3];
                table[key] = entry[7];
            }
        }
        TransTable(TransTable const&);
        void operator=(TransTable const&);

    private:
        unsigned char table[0x10000];
};


class NormalizedUTF8Reader {
    public:
        NormalizedUTF8Reader(char *pos) : pos(pos) {
        }
        inline unsigned char nextChar() {
            while (true) {
                unsigned char c = (unsigned char) *pos;
                if (c == '\n' || c == 0)
                    return 0;
                if (c <= 0x7f) {
                    pos ++;
                    return c;
                }
                if ((c & 0xe0) == 0xc0) {
                    unsigned int wc = *(pos + 1) & 0x3f;
                    wc |= (((unsigned int) c) & 0x1f) << 6;
                    pos += 2;
                    /* TODO does this tolower work? */
                    return TransTable::translate(use_facet<ctype<wchar_t>>(locale()).tolower((wchar_t) wc));
                }
                pos ++;
            }
        }

    private:
        char *pos;
};

class WordStart {
    public:
        WordStart(char *position) : position(position) {}

        inline bool operator<(const WordStart &other) const {
            NormalizedUTF8Reader thisReader(position), otherReader(other.position);
            while (true) {
                unsigned char c1 = thisReader.nextChar();
                unsigned char c2 = otherReader.nextChar();
                if (c1 == 0 && c2 == 0)
                    return false;
                if (c1 < c2)
                    return true;
                if (c1 > c2)
                    return false;
            }
        }

        inline unsigned int getPosInBuffer(char *buffer) const {
            return position - buffer;
        }

        string getNormalizedValue() const {
            NormalizedUTF8Reader reader(position);
            string s("");
            wchar_t wc;
            while ((wc = reader.nextChar()) != 0) {
                s += wc;
            }
            return s;
        }
        char *getPosition() {
            return position;
        }
    private:
        char *position;
};

inline bool isWord(char c) {
    return ('a' <= c && c <= 'z') || ('A' <= c && c <= 'Z') ||
           ('0' <= c && c <= '9');
}

void findWordStarts(char *buffer, int length, vector<WordStart> &wordStarts)
{
    /* TODO considers any non-ascii character to be a non-word character */
    /* TODO leave out the first word, it is already in the other index */
    bool previousWasWord = false;
    bool firstWord = true;
    for (int position = 13; position < length; position ++) {
        if (buffer[position] == '\n') {
            position += 13;
            previousWasWord = false;
            firstWord = true;
        }
        char c = buffer[position];
        if (isWord(c) && !previousWasWord) {
            if (firstWord) {
                firstWord = false;
            } else {
                wordStarts.push_back(WordStart(buffer + position));
            }
        }
        previousWasWord = isWord(c);
    }
}

char *readFile(const char *name, int &length_ret)
{
    ifstream in(name, ios::binary | ios::in);
    in.seekg(0, ios::end);
    int length = in.tellg();
    in.seekg(0, ios::beg);

    char *buffer = new char[length];
    in.read(buffer, length);
    in.close();

    length_ret = length;
    return buffer;
}

char *readFile(const char *name)
{
    int length;
    return readFile(name, length);
}

void createIndex(char *buffer, int length)
{
    vector<WordStart> wordStarts;

    // initialize TransTable
    TransTable::translate((wchar_t)0);

    cout << "Finding word starts..." << endl;
    findWordStarts(buffer, length, wordStarts);
    cout << "Number of word starts: " << wordStarts.size() << endl;
    cout << "Sorting..." << endl;

    sort(wordStarts.begin(), wordStarts.end());
    
    cout << "Writing..." << endl;

    ofstream out("titles_search.idx", ios::binary | ios::out);
    for (WordStart ws : wordStarts) {
        unsigned int pos = ws.getPosInBuffer(buffer);
        unsigned char values[4];
        for (int i = 0; i < 4; i ++, pos >>= 8)
            values[i] = pos & 0xff;
        out.write(reinterpret_cast<char *>(values), 4);
    }
}

void searchIndex(char *titleBuffer, char *query)
{
    int length = 0;
    char *index = readFile("titles_search.idx", length);
    vector<WordStart> wordStarts;
    for (int i = 0; i < length; i += 4) {
        unsigned int value = 0;
        for (int j = 3; j >= 0; j --)
            value = (value << 8) | (unsigned char) index[i + j];
        wordStarts.push_back(WordStart(titleBuffer + value));
    }

    vector<WordStart>::iterator it = lower_bound(wordStarts.begin(), wordStarts.end(), WordStart(query));

    for (int i = 0; i < 20; i ++) {
        cout << it->getPosition() << endl;
        ++it;
    }

    delete[] index;
}


int main(int argc, char *argv[])
{
    int length;
    char *buffer = readFile("titles.idx", length);

    if (argc <= 1) {
        createIndex(buffer, length);
    } else {
        searchIndex(buffer, argv[1]);
    }

    delete[] buffer;
    return 0;
}
