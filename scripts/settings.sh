#!/bin/bash

DUMPATHOMESERVER="http://dumpathome.evopedia.info:8085"
#DUMPATHOMESERVER="http://dumpathome.evopedia.info:80"

REPODIR=$(readlink -f $(dirname $0)/..)
SCRIPTDIR="$REPODIR/scripts"
export PATH="$PATH:$SCRIPTDIR"

DUMPSTATEDIR="$REPODIR/state"

CURL="curl -H 'Expect' -s -S"

# mediawiki mysql settings
dbuser=root
password=
database=wikidb

# SUDOCMD is used to access the database files directly
SUDOCMD=""
MYSQLDIR="$REPODIR/mysql"
DATABASEDIR="$MYSQLDIR/data/$database"
LIBDIR="$REPODIR/evopedia/"
MEDIAWIKIDIR="$REPODIR/mediawiki-1.21.2/"
MYSQL_OPTS="--defaults-file=$MYSQLDIR/my.cnf"

# temporary and target directories
DUMPDIR="$REPODIR/data"
SOURCEDUMPDIR="$DUMPDIR/source_dumps"
IMPORTTEMPDIR="$DUMPDIR/import_temp"
DESTDUMPTEMPDIR="$DUMPDIR/dumps_temp"
RENDERTEMPDIR="$DUMPDIR/temp"
DESTDUMPDIR="$DUMPDIR/dumps"
PACKAGEDIR="$DUMPDIR/package"
# only used on the server
LOCALDBDIR="$DUMPDIR/databases"
