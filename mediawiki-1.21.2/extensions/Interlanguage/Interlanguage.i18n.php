<?php
/**
 * Internationalisation file for Interlanguage extension.
 *
 * @file
 * @ingroup Extensions
 */

$messages = array();

$messages['en'] = array(
	'interlanguage-desc' => 'Grabs interlanguage links from another wiki',
	'interlanguage-pagelinksexplanation' => 'Pages with interlanguage links:',
	'interlanguage-editlinks' => 'Edit links',
);

/** Message documentation (Message documentation)
 * @author Fryed-peach
 * @author Nikola Smolenski
 * @author Purodha
 * @author Shirayuki
 */
$messages['qqq'] = array(
	'interlanguage-desc' => '{{desc|name=Interlanguage|url=http://www.mediawiki.org/wiki/Extension:Interlanguage}}',
	'interlanguage-editlinks' => '[[Image:InterlanguageLinks-Sidebar-Monobook.png|right]]
This is a link to a page where interlanguage links of the current page can be edited. See the image on the right for how it should look like.
{{Identical|Edit link}}',
);

/** Afrikaans (Afrikaans)
 * @author Naudefj
 */
$messages['af'] = array(
	'interlanguage-desc' => "Gryp tussentaalskakels vanaf 'n ander wiki",
);

/** Arabic (العربية)
 * @author Meno25
 */
$messages['ar'] = array(
	'interlanguage-desc' => 'يجلب الوصلات بين اللغات من ويكي آخر',
);

/** Asturian (asturianu)
 * @author Xuacu
 */
$messages['ast'] = array(
	'interlanguage-desc' => "Recueye enllaces interllingüísticos d'otra wiki",
	'interlanguage-pagelinksexplanation' => 'Páxines con enllaces interllingüísticos:',
	'interlanguage-editlinks' => 'Editar los enllaces',
);

/** Azerbaijani (azərbaycanca)
 * @author Sortilegus
 * @author Wertuose
 */
$messages['az'] = array(
	'interlanguage-desc' => 'Digər vikidən dillərarası keçidləri tutur',
	'interlanguage-pagelinksexplanation' => 'Dillərarası keçidləri olan səhifələr:',
	'interlanguage-editlinks' => 'Keçidləri redaktə et',
);

/** South Azerbaijani (تورکجه)
 * @author Mousa
 */
$messages['azb'] = array(
	'interlanguage-desc' => 'آیری بیر ویکی‌دن، دیل‌لر-آراسی باغلانتیلاری آلیر',
	'interlanguage-pagelinksexplanation' => 'دیل‌لر-آراسی باغلانتیلاری اولان صحیفه‌لر:',
	'interlanguage-editlinks' => 'باغلانتیلاری دَییشدیر',
);

/** Belarusian (беларуская)
 * @author Хомелка
 */
$messages['be'] = array(
	'interlanguage-desc' => 'Пераносіць міжмоўныя спасылкі з іншых вікі',
);

/** Belarusian (Taraškievica orthography) (беларуская (тарашкевіца)‎)
 * @author Jim-by
 * @author Wizardist
 */
$messages['be-tarask'] = array(
	'interlanguage-desc' => 'Захоплівае міжмоўныя спасылкі зь іншай вікі',
	'interlanguage-pagelinksexplanation' => 'Старонкі са спасылкамі на іншыя моўныя вэрсіі:',
	'interlanguage-editlinks' => 'Рэдагаваць спасылкі',
);

/** Bulgarian (български)
 * @author Spiritia
 */
$messages['bg'] = array(
	'interlanguage-desc' => 'Заимства междуезикови препратки от друго уики',
	'interlanguage-pagelinksexplanation' => 'Страници с междуезикови препратки:',
	'interlanguage-editlinks' => 'Редактиране',
);

/** Bengali (বাংলা)
 * @author Wikitanvir
 */
$messages['bn'] = array(
	'interlanguage-editlinks' => 'সংযোগ সম্পাদনা',
);

/** Breton (brezhoneg)
 * @author Fulup
 */
$messages['br'] = array(
	'interlanguage-desc' => 'Talvezout a ra da zastum liammoù etreyezhoù adalek ur wiki all',
	'interlanguage-pagelinksexplanation' => 'Pejannoù enno liammoù etreezhoù :',
	'interlanguage-editlinks' => 'Kemmañ al liammoù',
);

/** Bosnian (bosanski)
 * @author CERminator
 */
$messages['bs'] = array(
	'interlanguage-desc' => 'Preuzima međujezičke linkove sa druge wiki',
	'interlanguage-pagelinksexplanation' => 'Stranice sa interwiki linkovima:',
	'interlanguage-editlinks' => 'Uredi linkove',
);

/** Catalan (català)
 * @author Paucabot
 */
$messages['ca'] = array(
	'interlanguage-desc' => "Recull interwikis d'una altra wiki",
);

/** Sorani Kurdish (کوردی)
 * @author Calak
 */
$messages['ckb'] = array(
	'interlanguage-editlinks' => 'بەستەرەکان دەستکاری بکە',
);

/** Czech (česky)
 * @author Chmee2
 */
$messages['cs'] = array(
	'interlanguage-editlinks' => 'Upravit odkaz',
);

/** German (Deutsch)
 * @author Kghbln
 * @author Les Meloures
 * @author Marcel083
 * @author Metalhead64
 * @author PasO
 */
$messages['de'] = array(
	'interlanguage-desc' => 'Ermöglicht den Abruf von Links zu verbundenen Wikis in anderen Sprachen',
	'interlanguage-pagelinksexplanation' => 'Seiten mit Links zu verbundenen Wikis in anderen Sprachen',
	'interlanguage-editlinks' => 'Links bearbeiten',
);

/** Lower Sorbian (dolnoserbski)
 * @author Michawiki
 */
$messages['dsb'] = array(
	'interlanguage-desc' => 'Zběra mjazyrěcne wótkaze z drugego wikija',
	'interlanguage-pagelinksexplanation' => 'Boki z wótkazami mjazy rozdźělnymi rěcami',
	'interlanguage-editlinks' => 'Wótkaze wobźěłaś',
);

/** Greek (Ελληνικά)
 * @author Glavkos
 * @author Omnipaedista
 * @author ZaDiak
 */
$messages['el'] = array(
	'interlanguage-desc' => 'Επιλέγει τυχαία συνδέσμους μεταξύ γλωσσών από ένα άλλο wiki',
	'interlanguage-editlinks' => 'Επεξεργασία συνδέσμων',
);

/** Spanish (español)
 * @author Armando-Martin
 * @author Crazymadlover
 * @author Translationista
 */
$messages['es'] = array(
	'interlanguage-desc' => 'Coge vínculos interidiomáticos de otro wiki',
	'interlanguage-pagelinksexplanation' => 'Páginas con enlaces interidioma:',
	'interlanguage-editlinks' => 'Editar los enlaces',
);

/** Basque (euskara)
 * @author An13sa
 * @author Theklan
 */
$messages['eu'] = array(
	'interlanguage-desc' => 'Beste wiki bateko hizkuntzen arteko loturak hartzen ditu',
	'interlanguage-editlinks' => 'Aldatu loturak',
);

/** Finnish (suomi)
 * @author Beluga
 * @author Centerlink
 * @author Crt
 * @author Nike
 */
$messages['fi'] = array(
	'interlanguage-desc' => 'Ottaa kieltenväliset linkit toisesta wikistä.',
	'interlanguage-pagelinksexplanation' => 'Sivut, joilla on kieltenvälisiä linkkejä:',
	'interlanguage-editlinks' => 'Muokkaa linkkejä',
);

/** French (français)
 * @author IAlex
 * @author Peter17
 * @author Verdy p
 */
$messages['fr'] = array(
	'interlanguage-desc' => 'Permet de collecter les liens interlangues d’un autre wiki',
	'interlanguage-pagelinksexplanation' => 'Pages avec des liens inter-langues :',
	'interlanguage-editlinks' => 'Modifier les liens',
);

/** Franco-Provençal (arpetan)
 * @author ChrisPtDe
 */
$messages['frp'] = array(
	'interlanguage-desc' => 'Pèrmèt de ramassar los lims entèrlengoues d’un ôtro vouiqui.',
	'interlanguage-pagelinksexplanation' => 'Pâges avouéc des lims entèrlengoues',
	'interlanguage-editlinks' => 'Changiér los lims',
);

/** Galician (galego)
 * @author Toliño
 */
$messages['gl'] = array(
	'interlanguage-desc' => 'Permite a obtención de ligazóns interlingüísticas desde outro wiki',
	'interlanguage-pagelinksexplanation' => 'Páxinas con ligazóns interlingüísticas:',
	'interlanguage-editlinks' => 'Editar as ligazóns',
);

/** Swiss German (Alemannisch)
 * @author Als-Chlämens
 * @author Als-Holder
 */
$messages['gsw'] = array(
	'interlanguage-desc' => 'Holt Interwikigleicher us eme andere Wiki',
	'interlanguage-pagelinksexplanation' => 'Syte mit Links zu andere Sprache',
	'interlanguage-editlinks' => 'Links bearbeite',
);

/** Manx (Gaelg)
 * @author MacTire02
 */
$messages['gv'] = array(
	'interlanguage-desc' => "T'eh shoh goaill greim er kianglaghyn eddyr-wiki veih wiki elley",
);

/** Hebrew (עברית)
 * @author Amire80
 * @author Rotemliss
 * @author YaronSh
 */
$messages['he'] = array(
	'interlanguage-desc' => 'קבלת קישורים בין־לשוניים ("בינוויקי") מאתר ויקי אחר',
	'interlanguage-pagelinksexplanation' => 'דפים עם קישורים בין־לשוניים:',
	'interlanguage-editlinks' => 'עריכת קישורים',
);

/** Hiligaynon (Ilonggo)
 * @author Tagimata
 */
$messages['hil'] = array(
	'interlanguage-desc' => 'Gakuha sang interlengwahe halin sa lain nga wiki',
);

/** Croatian (hrvatski)
 * @author Mnalis
 * @author SpeedyGonsales
 */
$messages['hr'] = array(
	'interlanguage-desc' => 'Dohvati međujezične veze sa drugog wikija',
	'interlanguage-pagelinksexplanation' => 'Stranice s međuwiki poveznicama:',
);

/** Upper Sorbian (hornjoserbsce)
 * @author Michawiki
 */
$messages['hsb'] = array(
	'interlanguage-desc' => 'Zběra mjezyrěčne wotkazy z druheho wikija',
	'interlanguage-pagelinksexplanation' => 'Strony z mjezyrěčnymi wotkazami',
	'interlanguage-editlinks' => 'Wotkazy wobdźěłać',
);

/** Hungarian (magyar)
 * @author Dj
 * @author Glanthor Reviol
 */
$messages['hu'] = array(
	'interlanguage-desc' => 'Nyelvközi hivatkozások átvétele más wikiből',
	'interlanguage-pagelinksexplanation' => 'Nyelvközi hivatkozással rendelkező lapok:',
	'interlanguage-editlinks' => 'Hivatkozások szerkesztése',
);

/** Armenian (Հայերեն)
 * @author Vadgt
 */
$messages['hy'] = array(
	'interlanguage-editlinks' => 'Փոխել հղումները',
);

/** Interlingua (interlingua)
 * @author McDutchie
 */
$messages['ia'] = array(
	'interlanguage-desc' => 'Collige ligamines interlingual ex un altere wiki',
	'interlanguage-pagelinksexplanation' => 'Paginas con ligamines de linguas:',
	'interlanguage-editlinks' => 'Modificar ligamines',
);

/** Indonesian (Bahasa Indonesia)
 * @author Bennylin
 * @author Farras
 * @author IvanLanin
 */
$messages['id'] = array(
	'interlanguage-desc' => 'Ambil pranala antarbahasa dari wiki lain',
	'interlanguage-pagelinksexplanation' => 'Halaman dengan pranala antarbahasa:',
	'interlanguage-editlinks' => 'Sunting pranala',
);

/** Igbo (Igbo)
 * @author Ukabia
 */
$messages['ig'] = array(
	'interlanguage-desc' => 'Na kpá jikodo MétúáríAsụsụ shí wiki ozor',
);

/** Icelandic (íslenska)
 * @author Snævar
 */
$messages['is'] = array(
	'interlanguage-desc' => 'Sækir tungumálatengla frá öðrum wiki',
	'interlanguage-pagelinksexplanation' => 'Síður með tungumálatengla:',
	'interlanguage-editlinks' => 'Breyta tenglum',
);

/** Italian (italiano)
 * @author Beta16
 * @author Darth Kule
 */
$messages['it'] = array(
	'interlanguage-desc' => 'Raccoglie link interlingua da un altro sito wiki',
	'interlanguage-pagelinksexplanation' => 'Pagine con link interlingua:',
	'interlanguage-editlinks' => 'Modifica link',
);

/** Japanese (日本語)
 * @author Aotake
 * @author Schu
 * @author Shirayuki
 * @author 青子守歌
 */
$messages['ja'] = array(
	'interlanguage-desc' => '言語間リンクを他のウィキから取得する',
	'interlanguage-pagelinksexplanation' => '言語間リンクがあるページ:',
	'interlanguage-editlinks' => 'リンクの編集',
);

/** Georgian (ქართული)
 * @author David1010
 */
$messages['ka'] = array(
	'interlanguage-pagelinksexplanation' => 'გვერდები ენათშორისი ბმულებით:',
	'interlanguage-editlinks' => 'ბმულების რედაქტირება',
);

/** Khmer (ភាសាខ្មែរ)
 * @author គីមស៊្រុន
 */
$messages['km'] = array(
	'interlanguage-desc' => 'ចាប់យកតំណភ្ជាប់អន្តរវិគីពីវីគីមួយផ្សេងទៀត',
	'interlanguage-pagelinksexplanation' => 'ទំព័រដែលមានតំណភ្ជាប់អន្តរភាសា៖',
	'interlanguage-editlinks' => 'កែប្រែតំណភ្ជាប់',
);

/** Korean (한국어)
 * @author 아라
 */
$messages['ko'] = array(
	'interlanguage-desc' => '다른 위키에서 인터언어 링크 가져오기',
	'interlanguage-pagelinksexplanation' => '인터언어 링크가 있는 문서:',
	'interlanguage-editlinks' => '링크 편집',
);

/** Karachay-Balkar (къарачай-малкъар)
 * @author Iltever
 */
$messages['krc'] = array(
	'interlanguage-desc' => 'Башха викиден тилле арасы джибериулени алады',
	'interlanguage-pagelinksexplanation' => 'Тилле арасы джибриулери болгъан бетле:',
	'interlanguage-editlinks' => 'Джибериулени тюрлендир',
);

/** Colognian (Ripoarisch)
 * @author Purodha
 */
$messages['ksh'] = array(
	'interlanguage-desc' => 'Hollt de Lenks zwesche de Shprooche vun enem ander Wiki.',
	'interlanguage-pagelinksexplanation' => 'Atikele met Lenks op ander Shprooche',
	'interlanguage-editlinks' => 'de Schproochelengks ändere',
);

/** Kurdish (Latin script) (Kurdî (latînî)‎)
 * @author George Animal
 */
$messages['ku-latn'] = array(
	'interlanguage-editlinks' => 'Girêdanan biguherîne',
);

/** Luxembourgish (Lëtzebuergesch)
 * @author Les Meloures
 * @author Robby
 */
$messages['lb'] = array(
	'interlanguage-desc' => "Erlaabt et d'Linken tëschent Sproochen vun enger anerer Wiki z'iwwerhuelen",
	'interlanguage-pagelinksexplanation' => 'Säite mat Linke mat anere Sproochen:',
	'interlanguage-editlinks' => 'Linken änneren',
);

/** Lithuanian (lietuvių)
 * @author Eitvys200
 */
$messages['lt'] = array(
	'interlanguage-editlinks' => 'Redaguoti nuorodas',
);

/** Latvian (latviešu)
 * @author Xil
 */
$messages['lv'] = array(
	'interlanguage-desc' => 'Paņem starpvalodu saites no citas wiki',
);

/** Macedonian (македонски)
 * @author Bjankuloski06
 */
$messages['mk'] = array(
	'interlanguage-desc' => 'Презема меѓујазични врски од друго вики',
	'interlanguage-pagelinksexplanation' => 'Страници со меѓујазични врски:',
	'interlanguage-editlinks' => 'Уреди врски',
);

/** Malayalam (മലയാളം)
 * @author Praveenp
 */
$messages['ml'] = array(
	'interlanguage-desc' => 'മറ്റൊരു വിക്കിയിൽ നിന്നും അന്തർഭാഷാ കണ്ണികൾ എടുക്കുക',
);

/** Malay (Bahasa Melayu)
 * @author Anakmalaysia
 */
$messages['ms'] = array(
	'interlanguage-desc' => 'Mengambil pautan antara bahasa dari wiki lain',
	'interlanguage-pagelinksexplanation' => 'Laman dengan pautan antara bahasa:',
	'interlanguage-editlinks' => 'Sunting pautan',
);

/** Norwegian Bokmål (norsk (bokmål)‎)
 * @author Nghtwlkr
 */
$messages['nb'] = array(
	'interlanguage-desc' => 'Henter språklenker fra andre wikier',
	'interlanguage-pagelinksexplanation' => 'Sider med språklenker:',
	'interlanguage-editlinks' => 'Rediger lenker',
);

/** Dutch (Nederlands)
 * @author Siebrand
 */
$messages['nl'] = array(
	'interlanguage-desc' => 'Haalt intertaalkoppelingen uit een andere wiki',
	'interlanguage-pagelinksexplanation' => "Pagina's met intertaalkoppelingen:",
	'interlanguage-editlinks' => 'Koppelingen bewerken',
);

/** Norwegian Nynorsk (norsk (nynorsk)‎)
 * @author Harald Khan
 * @author Njardarlogar
 */
$messages['nn'] = array(
	'interlanguage-desc' => 'Hentar språklenkjer frå ein annan wiki',
);

/** Occitan (occitan)
 * @author Cedric31
 */
$messages['oc'] = array(
	'interlanguage-desc' => 'Permet de collectar los ligams interlengas d’un autre wiki',
);

/** Polish (polski)
 * @author Sp5uhe
 * @author Woytecr
 */
$messages['pl'] = array(
	'interlanguage-desc' => 'Przechwytuje linki do projektów w innych językach z innej wiki',
	'interlanguage-pagelinksexplanation' => 'Strony z linkami do wersji w innych językach:',
	'interlanguage-editlinks' => 'Zarządzaj linkami',
);

/** Piedmontese (Piemontèis)
 * @author Borichèt
 * @author Dragonòt
 */
$messages['pms'] = array(
	'interlanguage-desc' => 'A cheuj anliure antërlenga da àutre wiki',
	'interlanguage-pagelinksexplanation' => "Pàgine con dj'anliure antra lenghe:",
	'interlanguage-editlinks' => "Modifiché j'anliure",
);

/** Pashto (پښتو)
 * @author Ahmed-Najib-Biabani-Ibrahimkhel
 */
$messages['ps'] = array(
	'interlanguage-desc' => 'له يوه بل ويکي نه د ژبو خپلمنځي تړنې را اخلي',
	'interlanguage-editlinks' => 'تړنې سمول',
);

/** Portuguese (português)
 * @author Giro720
 * @author Hamilton Abreu
 */
$messages['pt'] = array(
	'interlanguage-desc' => 'Extrai os links interlínguas de outra wiki',
	'interlanguage-pagelinksexplanation' => 'Páginas com links interlínguas:',
	'interlanguage-editlinks' => 'Editar Links',
);

/** Brazilian Portuguese (português do Brasil)
 * @author Eduardo.mps
 * @author Giro720
 */
$messages['pt-br'] = array(
	'interlanguage-desc' => 'Obtém interwikis de idiomas a partir de outra wiki',
	'interlanguage-pagelinksexplanation' => 'Páginas com links interidiomas:',
	'interlanguage-editlinks' => 'Editar links',
);

/** Quechua (Runa Simi)
 * @author AlimanRuna
 */
$messages['qu'] = array(
	'interlanguage-editlinks' => "T'inkikunata llamk'apuy",
);

/** Romanian (română)
 * @author Firilacroco
 * @author Minisarm
 */
$messages['ro'] = array(
	'interlanguage-pagelinksexplanation' => 'Pagini cu legături interlinguale:',
	'interlanguage-editlinks' => 'Modifică legăturile',
);

/** tarandíne (tarandíne)
 * @author Joetaras
 */
$messages['roa-tara'] = array(
	'interlanguage-desc' => 'Pigghie le collegaminde inderlènghe da otre Uicchi',
	'interlanguage-pagelinksexplanation' => 'Pàggen cu le colledaminde a otre Uicchi:',
	'interlanguage-editlinks' => 'Cange le collegaminde',
);

/** Russian (русский)
 * @author MaxSem
 * @author Prima klasy4na
 * @author Александр Сигачёв
 */
$messages['ru'] = array(
	'interlanguage-desc' => 'Захватывает межъязыковые ссылки из другой вики',
	'interlanguage-pagelinksexplanation' => 'Страницы с межъязыковыми ссылками:',
	'interlanguage-editlinks' => 'Править ссылки',
);

/** Sakha (саха тыла)
 * @author HalanTul
 */
$messages['sah'] = array(
	'interlanguage-desc' => 'Атын тылларынан суруллубут биикилэртэн сигэлэри булар',
);

/** Sinhala (සිංහල)
 * @author පසිඳු කාවින්ද
 */
$messages['si'] = array(
	'interlanguage-pagelinksexplanation' => 'අන්තර්භාෂාමය සබැඳි සහිත පිටු:',
	'interlanguage-editlinks' => 'සබැඳිය සංස්කරණය කරන්න',
);

/** Slovak (slovenčina)
 * @author Helix84
 */
$messages['sk'] = array(
	'interlanguage-desc' => 'Získa medzijazykové odkazy z inej wiki',
	'interlanguage-pagelinksexplanation' => 'Stránky s jazykovými odkazmi:',
	'interlanguage-editlinks' => 'Upraviť odkazy',
);

/** Slovenian (slovenščina)
 * @author Dbc334
 */
$messages['sl'] = array(
	'interlanguage-desc' => 'Zagrabi medjezikovne povezave z drugega wikija',
	'interlanguage-pagelinksexplanation' => 'Strani z medjezikovnimi povezavami:',
	'interlanguage-editlinks' => 'Uredi povezave',
);

/** Serbian (Cyrillic script) (српски (ћирилица)‎)
 * @author Михајло Анђелковић
 */
$messages['sr-ec'] = array(
	'interlanguage-desc' => 'Преузима међујезичке везе са другог пројекта',
	'interlanguage-pagelinksexplanation' => 'Странице са међујезичким везама:',
	'interlanguage-editlinks' => 'Уреди везе',
);

/** Serbian (Latin script) (srpski (latinica)‎)
 */
$messages['sr-el'] = array(
	'interlanguage-desc' => 'Preuzima međuviki sa drugog projekta',
	'interlanguage-pagelinksexplanation' => 'Stranice sa međujezičkim vezama:',
	'interlanguage-editlinks' => 'Uredi veze',
);

/** Sundanese (Basa Sunda)
 * @author Kandar
 */
$messages['su'] = array(
	'interlanguage-desc' => 'Nyokot tutumbu antarbasa ti wiki séjén',
);

/** Swedish (svenska)
 * @author Per
 * @author WikiPhoenix
 */
$messages['sv'] = array(
	'interlanguage-desc' => 'Hämtar språklänkar från andra wikis',
	'interlanguage-pagelinksexplanation' => 'Sidor med språklänkar:',
	'interlanguage-editlinks' => 'Redigera länkar',
);

/** Tamil (தமிழ்)
 * @author Karthi.dr
 * @author Shanmugamp7
 */
$messages['ta'] = array(
	'interlanguage-pagelinksexplanation' => 'பிற மொழி இணைப்புள்ள பக்கங்கள்:',
	'interlanguage-editlinks' => 'இணைப்புக்களைத் தொகு',
);

/** Telugu (తెలుగు)
 * @author Kiranmayee
 */
$messages['te'] = array(
	'interlanguage-desc' => 'అంతర భాషా లింకులను వేరొక వికీ నుంచి సంపాదిస్తుంది',
);

/** Thai (ไทย)
 * @author Nullzero
 */
$messages['th'] = array(
	'interlanguage-editlinks' => 'แก้ไขลิงก์ข้ามภาษา',
);

/** Tagalog (Tagalog)
 * @author AnakngAraw
 */
$messages['tl'] = array(
	'interlanguage-desc' => 'Kumukuha ng mga kawing na pang-interwika mula sa ibang wiki',
	'interlanguage-pagelinksexplanation' => 'Mga pahinang may mga kawing na pang-ugnayan ng wika:',
	'interlanguage-editlinks' => 'Baguhin ang mga Kawing',
);

/** Turkish (Türkçe)
 * @author Emperyan
 * @author Joseph
 */
$messages['tr'] = array(
	'interlanguage-desc' => 'Başka bir vikiden dillerarası bağlantıları alır',
	'interlanguage-pagelinksexplanation' => 'Dillerarası bağlantıları olan sayfalar:',
	'interlanguage-editlinks' => 'Bağlantıları düzenle',
);

/** Ukrainian (українська)
 * @author Prima klasy4na
 * @author Тест
 */
$messages['uk'] = array(
	'interlanguage-desc' => 'Імпортує міжмовні посилання з іншої вікі',
	'interlanguage-pagelinksexplanation' => 'Сторінки з посиланнями на іншомовні версії:',
	'interlanguage-editlinks' => 'Редагувати інтервікі',
);

/** Urdu (اردو)
 * @author පසිඳු කාවින්ද
 */
$messages['ur'] = array(
	'interlanguage-editlinks' => 'لنکس میں ترمیم کریں',
);

/** Uzbek (oʻzbekcha)
 * @author CoderSI
 * @author Sociologist
 */
$messages['uz'] = array(
	'interlanguage-pagelinksexplanation' => 'Tillararo havolalar mavjud sahifalar:',
	'interlanguage-editlinks' => 'Havolalarni tahrirlash',
);

/** vèneto (vèneto)
 * @author Frigotoni
 */
$messages['vec'] = array(
	'interlanguage-editlinks' => 'Canbia link',
);

/** Vietnamese (Tiếng Việt)
 * @author Minh Nguyen
 */
$messages['vi'] = array(
	'interlanguage-desc' => 'Lấy liên kết giữa ngôn ngữ từ wiki khác',
	'interlanguage-pagelinksexplanation' => 'Trang có liên kết ngoại ngữ',
	'interlanguage-editlinks' => 'Sửa đổi liên kết',
);

/** Yiddish (ייִדיש)
 * @author פוילישער
 */
$messages['yi'] = array(
	'interlanguage-desc' => 'נעמט אינטערשפראַך לינקען פון אַן אַנדער וויקי',
	'interlanguage-pagelinksexplanation' => 'בלעטער מיט אינטערשפראך לינקען:',
	'interlanguage-editlinks' => 'רעדאקטירן לינקען',
);

/** Simplified Chinese (中文（简体）‎)
 * @author Hydra
 * @author Linforest
 */
$messages['zh-hans'] = array(
	'interlanguage-desc' => '抓住从另一个维基语链接',
	'interlanguage-pagelinksexplanation' => '中介语的链接的页：',
	'interlanguage-editlinks' => '编辑链接',
);

/** Traditional Chinese (中文（繁體）‎)
 * @author Simon Shek
 */
$messages['zh-hant'] = array(
	'interlanguage-desc' => '抓住從另一個維基語鏈接',
	'interlanguage-pagelinksexplanation' => '中介語的鏈接的頁：',
	'interlanguage-editlinks' => '編輯連結',
);
