<?php
/**
 *
 */

$magicWords = array();

/** English (English) */
$magicWords['en'] = array(
	'interlanguage' => array( 0, 'interlanguage' ),
);

/** Afrikaans (Afrikaans) */
$magicWords['af'] = array(
	'interlanguage' => array( 0, 'tussentaal', 'interlanguage' ),
);

/** Arabic (العربية) */
$magicWords['ar'] = array(
	'interlanguage' => array( 0, 'بين_اللغات' ),
);

/** Egyptian Spoken Arabic (مصرى) */
$magicWords['arz'] = array(
	'interlanguage' => array( 0, 'بين_اللغات', 'interlanguage' ),
);

/** Breton (brezhoneg) */
$magicWords['br'] = array(
	'interlanguage' => array( 0, 'etreyezh' ),
);

/** Chechen (нохчийн) */
$magicWords['ce'] = array(
	'interlanguage' => array( 0, 'меттиюкъ', 'межязык', 'interlanguage' ),
);

/** German (Deutsch) */
$magicWords['de'] = array(
	'interlanguage' => array( 0, 'intersprache' ),
);

/** Zazaki (Zazaki) */
$magicWords['diq'] = array(
	'interlanguage' => array( 0, 'şarzıwan' ),
);

/** Esperanto (Esperanto) */
$magicWords['eo'] = array(
	'interlanguage' => array( 0, 'interlingvo' ),
);

/** Spanish (español) */
$magicWords['es'] = array(
	'interlanguage' => array( 0, 'interidioma' ),
);

/** Italian (italiano) */
$magicWords['it'] = array(
	'interlanguage' => array( 0, 'interlingua' ),
);

/** Japanese (日本語) */
$magicWords['ja'] = array(
	'interlanguage' => array( 0, '言語間' ),
);

/** Korean (한국어) */
$magicWords['ko'] = array(
	'interlanguage' => array( 0, '인터언어' ),
);

/** Ladino (Ladino) */
$magicWords['lad'] = array(
	'interlanguage' => array( 0, 'interlingua', 'interidioma', 'interlanguage' ),
);

/** Macedonian (македонски) */
$magicWords['mk'] = array(
	'interlanguage' => array( 0, 'меѓујазични' ),
);

/** Malayalam (മലയാളം) */
$magicWords['ml'] = array(
	'interlanguage' => array( 0, 'അന്തർഭാഷ' ),
);

/** Marathi (मराठी) */
$magicWords['mr'] = array(
	'interlanguage' => array( 0, 'आंतरभाषीय', 'interlanguage' ),
);

/** Dutch (Nederlands) */
$magicWords['nl'] = array(
	'interlanguage' => array( 0, 'intertaal' ),
);

/** Russian (русский) */
$magicWords['ru'] = array(
	'interlanguage' => array( 0, 'межязык' ),
);

/** Serbian (Latin script) (srpski (latinica)‎) */
$magicWords['sr-el'] = array(
	'interlanguage' => array( 0, 'unutrašnji_jezik' ),
);

/** Turkish (Türkçe) */
$magicWords['tr'] = array(
	'interlanguage' => array( 0, 'dillerarası' ),
);

/** Ukrainian (українська) */
$magicWords['uk'] = array(
	'interlanguage' => array( 0, 'міжмова' ),
);

/** Vietnamese (Tiếng Việt) */
$magicWords['vi'] = array(
	'interlanguage' => array( 0, 'liênngônngữ' ),
);