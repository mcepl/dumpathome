#!/bin/bash

SCRIPTDIR=$(dirname $0)
. "$SCRIPTDIR/settings.sh"

mkdir "$MYSQLDIR"/data >/dev/null 2>&1
mkdir "$MYSQLDIR"/data/mysql >/dev/null 2>&1

cp /usr/libexec/mysqld "$SCRIPTDIR" || exit 1

(
echo 'use mysql;'
cat /usr/share/mysql/mysql_system_tables.sql || exit 1
cat /usr/share/mysql/mysql_system_tables_data.sql || exit 1
cat /usr/share/mysql/fill_help_tables.sql || exit 1
) | "$SCRIPTDIR"/mysqld --datadir="$MYSQLDIR"/data/ --bootstrap --loose-skip-ndbcluster --max_allowed_packet=8M --default-storage-engine=innodb --net_buffer_length=16K >/dev/null 2>&1
