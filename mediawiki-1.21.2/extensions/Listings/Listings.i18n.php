<?php
/**
 * Internationalisation file for Listings extension.
 *
 * @package MediaWiki
 * @subpackage Extensions
*/

$messages = array();

$messages['en'] = array(
	'listings-desc'              => 'Add tags for listing locations',
	'listings-unknown'           => 'Unknown destination',
	'listings-phone'             => 'phone',
	'listings-phone-symbol'      => '☎',
	'listings-fax'               => 'fax',
	'listings-fax-symbol'        => '',
	'listings-email'             => 'e-mail',
	'listings-email-symbol'      => '',
	'listings-tollfree'          => 'toll-free',
	'listings-tollfree-symbol'   => '',
	'listings-checkin'           => 'Check-in: $1',
	'listings-checkout'          => 'check-out: $1',
	'listings-position'          => 'position: $1',
	'listings-position-template' => '',
);

/** Message documentation (Message documentation)
 * @author Shirayuki
 */
$messages['qqq'] = array(
	'listings-desc' => '{{desc}}',
	'listings-phone-symbol' => '{{Optional}}',
);

/** German (Deutsch)
 * @author Metalhead64
 */
$messages['de'] = array(
	'listings-desc' => 'Erweiterung zur Ausgabe von Ortsbeschreibungen',
	'listings-unknown' => 'Unbekannte Einrichtung',
	'listings-phone' => 'Telefon',
	'listings-fax' => 'Fax',
	'listings-email' => 'E-Mail',
	'listings-tollfree' => 'gebührenfrei',
	'listings-checkin' => 'Anmeldung: $1',
	'listings-checkout' => 'Abmeldung: $1',
	'listings-position' => 'Lage: $1',
);

/** Spanish (español)
 * @author Armando-Martin
 */
$messages['es'] = array(
	'listings-desc' => 'Añadir etiquetas para enumerar las localizaciones',
	'listings-unknown' => 'Destino desconocido',
	'listings-phone' => 'teléfono',
	'listings-fax' => 'fax',
	'listings-email' => 'correo electrónico',
	'listings-tollfree' => 'gratuito',
	'listings-checkin' => 'Registrarse: $1',
	'listings-checkout' => 'verificación: $1',
	'listings-position' => 'posición: $1',
);

/** French (français)
 * @author Gomoko
 */
$messages['fr'] = array(
	'listings-desc' => 'Ajouter les balises pour lister les emplacements',
	'listings-unknown' => 'Destination inconnue',
	'listings-phone' => 'tél.',
	'listings-fax' => 'fax',
	'listings-email' => 'email',
	'listings-tollfree' => 'gratuit',
	'listings-checkin' => 'Inscriptions: $1',
	'listings-checkout' => 'contrôle: $1',
	'listings-position' => 'situation: $1',
);

/** Galician (galego)
 * @author Toliño
 */
$messages['gl'] = array(
	'listings-desc' => 'Engade etiquetas para listar localizacións',
	'listings-unknown' => 'Destino descoñecido',
	'listings-phone' => 'teléfono',
	'listings-fax' => 'fax',
	'listings-email' => 'correo electrónico',
	'listings-tollfree' => 'gratuíto',
	'listings-checkin' => 'Inscrición: $1',
	'listings-checkout' => 'saída: $1',
	'listings-position' => 'posición: $1',
);

/** Upper Sorbian (hornjoserbsce)
 * @author Michawiki
 */
$messages['hsb'] = array(
	'listings-unknown' => 'Njeznaty cil',
	'listings-phone' => 'telefon',
	'listings-fax' => 'faks',
	'listings-email' => 'e-mejl',
	'listings-tollfree' => 'bjez popłatka',
	'listings-position' => 'pozicija: $1',
);

/** Italian (italiano)
 * @author Beta16
 */
$messages['it'] = array(
	'listings-desc' => 'Aggiunge tag per elenchi di posizioni',
	'listings-unknown' => 'Destinazione sconosciuta',
	'listings-phone' => 'tel.',
	'listings-fax' => 'fax',
	'listings-email' => 'email',
	'listings-tollfree' => 'gratis',
	'listings-checkin' => 'Check-in: $1',
	'listings-checkout' => 'check-out: $1',
	'listings-position' => 'posizione: $1',
);

/** Japanese (日本語)
 * @author Shirayuki
 */
$messages['ja'] = array(
	'listings-desc' => '場所を列挙するためのタグを追加する',
	'listings-phone' => '電話',
	'listings-fax' => 'FAX',
	'listings-email' => 'メール',
	'listings-tollfree' => 'フリーダイヤル',
	'listings-checkin' => 'チェックイン: $1',
	'listings-checkout' => 'チェックアウト: $1',
	'listings-position' => '位置: $1',
);

/** Luxembourgish (Lëtzebuergesch)
 * @author Robby
 */
$messages['lb'] = array(
	'listings-unknown' => 'Onbekannten Zil',
	'listings-fax' => 'Fax',
	'listings-email' => 'E-Mail',
	'listings-tollfree' => 'gratis',
	'listings-position' => 'Positioun: $1',
);

/** Macedonian (македонски)
 * @author Bjankuloski06
 */
$messages['mk'] = array(
	'listings-desc' => 'Додај ознаки за наведување на места',
	'listings-unknown' => 'Непознато одредиште',
	'listings-phone' => 'телефон',
	'listings-fax' => 'факс',
	'listings-email' => 'е-пошта',
	'listings-tollfree' => 'бесплатно',
	'listings-checkin' => 'Пријава: $1',
	'listings-checkout' => 'одјава: $1',
	'listings-position' => 'положба: $1',
);

/** Simplified Chinese (中文（简体）‎)
 * @author Yfdyh000
 */
$messages['zh-hans'] = array(
	'listings-unknown' => '未知目标',
	'listings-phone' => '电话',
	'listings-fax' => '传真',
	'listings-email' => '电子邮件',
	'listings-tollfree' => '免费电话',
	'listings-checkin' => '登入：$1',
	'listings-checkout' => '登出：$1',
	'listings-position' => '位置：$1',
);
