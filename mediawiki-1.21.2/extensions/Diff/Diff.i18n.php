<?php
/**
 * Internationalization file for the diff extension.
 *
 * @since 0.1
 *
 * @file
 * @ingroup Diff
 *
 * @licence GNU GPL v2+
 * @author Jeroen De Dauw < jeroendedauw@gmail.com >
 */

$messages = array();

/** English
 * @author Jeroen De Dauw
 */
$messages['en'] = array(
	'diff-desc' => 'Library with classes to represent diffs of structured data, as well as service objects to diff objects or patch them',
);

/** Message documentation (Message documentation)
 */
$messages['qqq'] = array(
	'diff-desc' => 'Diff library for structured data',
);

/** Arabic (العربية)
 * @author Abanima
 */
$messages['ar'] = array(
	'diff-desc' => 'مكتبة أصناف لتمثيل الفروق في البيانات ذات البنية بالإضافة إلى كائنات خدمية لإظهار الفروق بين الكائنات أو رقعها',
);

/** Asturian (asturianu)
 * @author Xuacu
 */
$messages['ast'] = array(
	'diff-desc' => 'Biblioteca de diferencies pa datos estructuraos', # Fuzzy
);

/** South Azerbaijani (تورکجه)
 * @author Mousa
 */
$messages['azb'] = array(
	'diff-desc' => 'قورولوشلو وئریلر اوچون موقاییسه کیتابخاناسی', # Fuzzy
);

/** Belarusian (Taraškievica orthography) (беларуская (тарашкевіца)‎)
 * @author Wizardist
 */
$messages['be-tarask'] = array(
	'diff-desc' => 'Бібліятэка для параўнаньня структураваных зьвестак', # Fuzzy
);

/** Czech (česky)
 * @author Mormegil
 */
$messages['cs'] = array(
	'diff-desc' => 'Knihovna pro zjišťování změn ve strukturovaných datech', # Fuzzy
);

/** Danish (dansk)
 * @author Kaare
 */
$messages['da'] = array(
	'diff-desc' => 'Diff-bibliotek til strukturerede data', # Fuzzy
);

/** German (Deutsch)
 * @author Kghbln
 * @author Metalhead64
 */
$messages['de'] = array(
	'diff-desc' => 'Stellt eine Bibliothek zum Darstellen sowie Dienstobjekte zum Ändern von Unterschieden zwischen strukturierten Daten bereit',
);

/** Lower Sorbian (dolnoserbski)
 * @author Michawiki
 */
$messages['dsb'] = array(
	'diff-desc' => 'Biblioteka rozdźělow za strukturěrowane daty', # Fuzzy
);

/** Spanish (español)
 * @author Armando-Martin
 */
$messages['es'] = array(
	'diff-desc' => 'Biblioteca de diferencias para datos estructurados', # Fuzzy
);

/** Basque (euskara)
 * @author Theklan
 */
$messages['eu'] = array(
	'diff-desc' => 'Diff liburutegia data egituratuetarako', # Fuzzy
);

/** Persian (فارسی)
 * @author Calak
 * @author Reza1615
 */
$messages['fa'] = array(
	'diff-desc' => 'کتابخانه با کلاس‌هایی برای نمایش دادن تفاوت‌های داده‌های ساخت‌یافته و هم‌چنین اشیای خدمات برای اشیا یا پچ متفاوت آن‌ها',
);

/** French (français)
 * @author Gomoko
 * @author Wyz
 */
$messages['fr'] = array(
	'diff-desc' => 'Bibliothèque avec des classes pour représenter les deltas ou les données structurées, tout comme les objets de service pour faire le delta entre objets ou les mettre à jour',
);

/** Galician (galego)
 * @author Toliño
 */
$messages['gl'] = array(
	'diff-desc' => 'Biblioteca con clases para representar diferenzas de datos estruturados, así como os obxectos de servizo aos obxectos de diferenzas ou parchealos',
);

/** Hebrew (עברית)
 * @author Amire80
 */
$messages['he'] = array(
	'diff-desc' => 'ספרייה להשוואת נתונים מבניים', # Fuzzy
);

/** Croatian (hrvatski)
 * @author SpeedyGonsales
 */
$messages['hr'] = array(
	'diff-desc' => 'Diff biblioteka funkcija za strukturirane podatke', # Fuzzy
);

/** Upper Sorbian (hornjoserbsce)
 * @author Michawiki
 */
$messages['hsb'] = array(
	'diff-desc' => 'Biblioteka rozdźělow za strukturowane daty', # Fuzzy
);

/** Indonesian (Bahasa Indonesia)
 * @author Farras
 */
$messages['id'] = array(
	'diff-desc' => 'Pustaka diff untuk data terstruktur', # Fuzzy
);

/** Iloko (Ilokano)
 * @author Lam-ang
 */
$messages['ilo'] = array(
	'diff-desc' => 'Biblioteka nga adda dagiti klase a mangirepresenta dagiti paggiddiatan iti naestruktura a datos, ken dagiti pay serbisio ti banag iti paggiddiatan a banbanag wenno takupan ida',
);

/** Italian (italiano)
 * @author Beta16
 * @author Darth Kule
 */
$messages['it'] = array(
	'diff-desc' => 'Libreria di classi per rappresentare le differenze di dati strutturati, come pure gli oggetti di servizio agli oggetti di confronto o di aggiornamento',
);

/** Japanese (日本語)
 * @author Fryed-peach
 * @author Shirayuki
 */
$messages['ja'] = array(
	'diff-desc' => '構造化データの差分を表現するクラスや、オブジェクトの差分をとったりパッチを当てたりするためのサービスオブジェクトを含むライブラリ',
);

/** Korean (한국어)
 * @author 아라
 */
$messages['ko'] = array(
	'diff-desc' => '구조화한 데이터의 차이점 뿐만 아니라 차이점 개체에 대한 서비스 개체 또는 패치를 대표하는 클래스로 된 라이브러리',
);

/** Colognian (Ripoarisch)
 * @author Purodha
 */
$messages['ksh'] = array(
	'diff-desc' => 'En Projrammbiblijoteek för Ongerscheide en Daate met Schtroktuur ze fenge.', # Fuzzy
);

/** Luxembourgish (Lëtzebuergesch)
 * @author Robby
 */
$messages['lb'] = array(
	'diff-desc' => 'Diff-Library fir strukturéiert Daten', # Fuzzy
);

/** Macedonian (македонски)
 * @author Bjankuloski06
 */
$messages['mk'] = array(
	'diff-desc' => 'иблиотека со класи што за претставување на верзиските разлики во структурираните податоци, а воедно и крпење на објектите со оглед на нивните разлики.',
);

/** Malay (Bahasa Melayu)
 * @author Anakmalaysia
 */
$messages['ms'] = array(
	'diff-desc' => 'Perpustakaan dengan kelas untuk mewakili perbezaan antara data berstruktur dan juga menservis objek untuk to memperbezakan atau menampung objek',
);

/** Norwegian Bokmål (norsk bokmål)
 * @author Jeblad
 */
$messages['nb'] = array(
	'diff-desc' => 'Biblioteket med klasser for å representere diffs av strukturerte data, i tillegg til serviceobjekter for diff og lapping',
);

/** Dutch (Nederlands)
 * @author Siebrand
 */
$messages['nl'] = array(
	'diff-desc' => 'Bibliotheek voor het weergeven van verschillen in gestructureerde gegevens',
);

/** Polish (polski)
 * @author BeginaFelicysym
 * @author Lazowik
 */
$messages['pl'] = array(
	'diff-desc' => 'Biblioteka porównywania danych strukturalnych', # Fuzzy
);

/** Piedmontese (Piemontèis)
 * @author Borichèt
 * @author Dragonòt
 */
$messages['pms'] = array(
	'diff-desc' => 'Libreria con class për rapresenté diferense ëd dat struturà, con oget ëd sërvissi a oget diff e sò pachèt',
);

/** Brazilian Portuguese (português do Brasil)
 * @author Jaideraf
 */
$messages['pt-br'] = array(
	'diff-desc' => 'Biblioteca Diff para dados estruturados', # Fuzzy
);

/** tarandíne (tarandíne)
 * @author Joetaras
 */
$messages['roa-tara'] = array(
	'diff-desc' => "Libbrerie cu le classe pe rappresendà le differenze 'mbrà le date strutturate, cumme le oggette de servizie ca sò diverse da le oggette o le accunzaminde lore",
);

/** Russian (русский)
 * @author DCamer
 */
$messages['ru'] = array(
	'diff-desc' => 'Библиотеки классов для представления diff-структурированных данных, а также подключение объектов к diff-объектам или их патчам',
);

/** Sinhala (සිංහල)
 * @author පසිඳු කාවින්ද
 */
$messages['si'] = array(
	'diff-desc' => 'ව්‍යූහගත දත්ත සඳහා Diff පුස්තකාලය', # Fuzzy
);

/** Swedish (svenska)
 * @author WikiPhoenix
 */
$messages['sv'] = array(
	'diff-desc' => 'Differansbibliotek för strukturerade data', # Fuzzy
);

/** Tagalog (Tagalog)
 * @author AnakngAraw
 */
$messages['tl'] = array(
	'diff-desc' => 'Aklatan ng pagkakaiba para sa datong may kayarian', # Fuzzy
);

/** Ukrainian (українська)
 * @author Base
 */
$messages['uk'] = array(
	'diff-desc' => 'Бібліотека для порівняння структурованих даних', # Fuzzy
);

/** Vietnamese (Tiếng Việt)
 * @author Minh Nguyen
 */
$messages['vi'] = array(
	'diff-desc' => 'Thư viện so sánh dữ liệu có cấu trúc', # Fuzzy
);

/** Traditional Chinese (中文（繁體）‎)
 * @author Justincheng12345
 */
$messages['zh-hant'] = array(
	'diff-desc' => '用於結構化資料的差別庫', # Fuzzy
);
