#!/bin/bash

SCRIPTDIR=$(dirname $0)

. "$SCRIPTDIR/settings.sh"
. "$SCRIPTDIR/dumplib.sh"

function sendlog_nosleep()
{
    file="$1"
    test -z "$file" && file="$REPODIR/log"
    echo "An error occurred. Sending error log to server..."
    $CURL -F user_id="$user_id" -F logfile=@"$file" "$DUMPATHOMESERVER/client_error_occured"
}

function sendlog()
{
    file="$1"
    test -z "$file" && file="$REPODIR/log"
    echo "An error occurred. Sending error log to server..."
    $CURL -F user_id="$user_id" -F logfile=@"$file" "$DUMPATHOMESERVER/client_error_occured"
    echo
    echo
    echo "Sleeping 200 seconds..."
    sleep 200
}

function choose_texvc()
{
    texvc64="$MEDIAWIKIDIR/extensions/Math/math/texvc_64static"
    texvc32="$MEDIAWIKIDIR/extensions/Math/math/texvc_32static"
    texvc="$MEDIAWIKIDIR/extensions/Math/math/texvc"
    #if uname -m | grep 64 
    #then
    #    cp "$texvc64" "$texvc"
    #else
        cp "$texvc32" "$texvc"
    #fi
}


function check_executables()
{
    error=""
    which curl > /dev/null || error="$error curl"
    which php > /dev/null || error="$error php"
    which tidy > /dev/null || error="$error tidy"
    which latex > /dev/null || error="$error latex"
    which dvipng > /dev/null || error="$error dvipng"
    which pngcrush > /dev/null || error="$error pngcrush"
    test -x '/usr/bin/perl' > /dev/null || error="$error /usr/bin/perl"
    test -x '/usr/libexec/mysqld' > /dev/null || error="$error /usr/libexec/mysqld"

    if [ -n "$error" ]
    then
        echo "Some programs needed for the dump process could not be found."
        echo "Please install the following programs:"
        echo $error
        exit 1
    fi

    texvc="$MEDIAWIKIDIR/extensions/Math/math/texvc"
    if [ ! $texvc >/dev/null ]
    then
        echo "$texvc could not be executed."
        echo "This can be caused by different libc versions."
        echo "Please check that."
        exit 1
    fi
    
    mkdir "$IMPORTTEMPDIR"/texvc 2>/dev/null
    texvcout=$("$texvc" \
        "$IMPORTTEMPDIR"/texvc/ \
        "$IMPORTTEMPDIR"/texvc/ \
        '\int x^2 \implies y' iso-8859-1)
    if [ "$texvcout" = '-' -o "$texvcout" = 'S' -o "$texvcout" = 'E' ]
    then
        echo "$texvc reported an error: $texvcout"
        echo "Please make sure that latex including tetex-extra is installed."
        exit 1
    fi

}

rm "$SCRIPTDIR"/dumpathome.sh_old >/dev/null 2>&1

choose_texvc
check_executables

echo "Dump@home started."

"$SCRIPTDIR/startmysql.sh" || exit 1

test -e "$DUMPSTATEDIR/user_id" || tr -dc A-Za-z0-9 < /dev/urandom | head -c32 > "$DUMPSTATEDIR/user_id"
test -e "$DUMPSTATEDIR/user" || echo -n "anonymous" > "$DUMPSTATEDIR/user"
test -e "$DUMPSTATEDIR/team" || echo -n "anonymous" > "$DUMPSTATEDIR/team"
test -e "$DUMPSTATEDIR/processes" || grep -c processor /proc/cpuinfo > "$DUMPSTATEDIR/processes"

user_id=$(cat "$DUMPSTATEDIR/user_id")
user=$(cat "$DUMPSTATEDIR/user")
team=$(cat "$DUMPSTATEDIR/team")
processes=$(cat "$DUMPSTATEDIR/processes")

echo "User ID: $user_id"
echo "User: $user"
echo "Team: $team"
echo "Number of processes: $processes"

if [ $# -ge 2 ]
then
    jobdesc=("$@")
    job_cmdline_override=1
    echo "Manual request to execute single job $@."
fi

while true
do
    logfile="$REPODIR"/log
    echo -n > "$logfile"
    (
    export TMP="$RENDERTEMPDIR"
    rm -r -- "$RENDERTEMPDIR"/* >/dev/null 2>&1
    if [ -z "$job_cmdline_override" ]
    then
	echo "Requesting new job."

	commonsdate=$(cat "$DUMPSTATEDIR/commonsdate" 2>/dev/null)
	wikilang=$(cat "$DUMPSTATEDIR/wikilang" 2>/dev/null)
	wikidate=$(cat "$DUMPSTATEDIR/wikidate" 2>/dev/null)
	client_revision=$(cat "$DUMPSTATEDIR/client_revision")
	echo $CURL -f "$DUMPATHOMESERVER/request_job" -F commonsdate="$commonsdate" -F wikidate="$wikidate" -F wikilang="$wikilang" -F clientrevision="$client_revision" -F user_id="$user_id" -F processes="$processes"
	jobdesc=($($CURL -f "$DUMPATHOMESERVER/request_job" -F commonsdate="$commonsdate" -F wikidate="$wikidate" -F wikilang="$wikilang" -F clientrevision="$client_revision" -F user_id="$user_id" -F processes="$processes"))
    fi
    job=${jobdesc[0]}

    checkMysqlRunning || (sendlog; continue)

    echo "User id: $user_id"
    echo "Got job: ${jobdesc[*]}."
    case "$job" in
        importcommons)
            date=${jobdesc[1]}
            echo "Downloading source dumps..."
            getCommonsImageSourceDumps "$date" || (sendlog; continue)

            echo "Discarding database..."
            discardDatabase || (sendlog; continue)
            echo -n "$date" > "$DUMPSTATEDIR"/commonsdate
            echo -n "" > "$DUMPSTATEDIR"/wikilang
            echo -n "" > "$DUMPSTATEDIR"/wikidate

            echo "Starting MySQL server..."
            "$SCRIPTDIR/startmysql.sh" || (sendlog; continue)

            echo "Importing source dumps..."
            echo -n "" > "$DUMPSTATEDIR"/commonsdate
            importCommons || (sendlog; continue)
            echo -n "$date" > "$DUMPSTATEDIR"/commonsdate

            echo "Shutting down database..."
            shutdownDatabase || (sendlog; continue)

            echo "Compressing database files..."
            (
            cd "$MYSQLDIR/data"
            nice tar -czf "$DESTDUMPDIR/db.tar.gz" *
            ) || (sendlog; continue)
            echo "Uploading database files..."
            #$CURL --retry 10 -F user_id="$user_id" -F user="$user" -F team="$team" -F db.tar.gz=@"$DESTDUMPDIR/db.tar.gz" "$DUMPATHOMESERVER/databases/commons/$date/upload" || (sendlog; continue)
            #rm "$DESTDUMPDIR/db.tar.gz"
            cp "$DESTDUMPDIR/db.tar.gz" "$LOCALDBDIR/db_commons_$date.tar.gz"
            echo "Job done."
            ;;
        importwiki)
            lang=${jobdesc[1]}
            date=${jobdesc[2]}
            echo "Downloading source dumps..."
            getSourceDumps "$lang" "$date" || (sendlog; continue)
            echo "Importing source dumps..."
            echo -n "" > "$DUMPSTATEDIR"/wikilang
            echo -n "" > "$DUMPSTATEDIR"/wikidate
            importLanguage "$lang" || (sendlog; continue)
            changeWikiSettings "$lang" || (sendlog; continue)
            echo -n "$lang" > "$DUMPSTATEDIR"/wikilang
            echo -n "$date" > "$DUMPSTATEDIR"/wikidate

            echo "Shutting down database..."
            shutdownDatabase || (sendlog; continue)

            echo "Compressing database files..."
            (
            cd "$MYSQLDIR/data"
            nice tar -czf "$DESTDUMPDIR/db.tar.gz" --exclude commons_\* *
            ) || (sendlog; continue)
            echo "Uploading database files..."
            #$CURL --retry 10 -F user_id="$user_id" -F user="$user" -F team="$team" -F db.tar.gz=@"$DESTDUMPDIR/db.tar.gz" "$DUMPATHOMESERVER/databases/$lang/$date/upload" || (sendlog; continue)
            #rm "$DESTDUMPDIR/db.tar.gz"
            cp "$DESTDUMPDIR/db.tar.gz" "$LOCALDBDIR"/db_"$lang"_"$date".tar.gz
            echo "Job done."
            ;;
        loadcommons)
            date=${jobdesc[1]}
            echo -n "" > "$DUMPSTATEDIR"/commonsdate
            echo -n "" > "$DUMPSTATEDIR"/wikilang
            echo -n "" > "$DUMPSTATEDIR"/wikidate
            echo "Terminating MySQL server..."
            discardDatabase || (sendlog; continue)
            (
            cd "$MYSQLDIR/data"
            echo "Downloading and extracting database files..."
            dbfile="db_commons-$date.tar.gz"
            (cd "$IMPORTTEMPDIR" && wget --progress=dot:mega -c "$DUMPATHOMESERVER/databases/commons/$date" -O "$dbfile" ) && \
                nice tar xzf "$IMPORTTEMPDIR/$dbfile"
            ) && "$SCRIPTDIR/startmysql.sh" && echo -n "$date" > "$DUMPSTATEDIR"/commonsdate && echo "Job done." || sendlog
            ;;
        loadcommons_local)
            date=${jobdesc[1]}
            echo -n "" > "$DUMPSTATEDIR"/commonsdate
            echo -n "" > "$DUMPSTATEDIR"/wikilang
            echo -n "" > "$DUMPSTATEDIR"/wikidate
            echo "Terminating MySQL server..."
            discardDatabase || (sendlog; continue)
            (
            cd "$MYSQLDIR/data"
            echo "Extracting database files..."
            nice tar xzf "$LOCALDBDIR/db_commons_$date.tar.gz"
            ) && "$SCRIPTDIR/startmysql.sh" && echo -n "$date" > "$DUMPSTATEDIR"/commonsdate && echo "Job done." || sendlog
            ;;
        loadwiki)
            lang=${jobdesc[1]}
            date=${jobdesc[2]}
            echo "Terminating MySQL server..."
            killall -9 mysqld > /dev/null 2>&1
            echo -n "" > "$DUMPSTATEDIR"/wikilang
            echo -n "" > "$DUMPSTATEDIR"/wikidate
            (
            cd "$MYSQLDIR/data"
            echo "Downloading and extracting database files..."
            dbfile="db_$lang-$date.tar.gz"
            (cd "$IMPORTTEMPDIR" && wget --progress=dot:mega -c "$DUMPATHOMESERVER/databases/$lang/$date" -O "$dbfile" ) && \
                nice tar xzf "$IMPORTTEMPDIR/$dbfile"
            ) && "$SCRIPTDIR/startmysql.sh" && changeWikiSettings "$lang" && echo -n "$lang" > "$DUMPSTATEDIR"/wikilang && echo -n "$date" > "$DUMPSTATEDIR"/wikidate && echo "Job done." || sendlog
            ;;
        loadwiki_local)
            lang=${jobdesc[1]}
            date=${jobdesc[2]}
            echo "Terminating MySQL server..."
            killall -9 mysqld > /dev/null 2>&1
            echo -n "" > "$DUMPSTATEDIR"/wikilang
            echo -n "" > "$DUMPSTATEDIR"/wikidate
            (
            cd "$MYSQLDIR/data"
            echo "Extracting database files..."
            nice tar xzf "$LOCALDBDIR"/db_"$lang"_"$date".tar.gz
            ) || (sendlog; continue)
            "$SCRIPTDIR/startmysql.sh" || (sendlog; continue)
            changeWikiSettings "$lang" || (sendlog; continue)
            echo -n "$lang" > "$DUMPSTATEDIR"/wikilang
            echo -n "$date" > "$DUMPSTATEDIR"/wikidate
            echo "Job done."
            ;;
        clear_import_temp)
            nice rm -r "$IMPORTTEMPDIR"/* || (sendlog; continue)
            echo "Job done."
            ;;
        createdump)
            slices=${jobdesc[*]:2}
            maxslices=${jobdesc[1]}

            ps x | grep mysqld | grep -v grep >/dev/null || "$SCRIPTDIR/startmysql.sh" || (sendlog; continue)
            checkMysqlRunning || (sendlog; continue)

            #createTablesForced
            #createTablesForced commons_
            rm -r "$DESTDUMPTEMPDIR" 2>/dev/null
            mkdir "$DESTDUMPTEMPDIR" 2>/dev/null
            echo "Rendering pages using ${#slices[*]} process(es)..."
            disown -a
            index=0
            for slice in $slices
            do
                (
                choose_texvc
                dumpWiki "$wikilang" "$slice" "$maxslices" "$index" || (sendlog; exit)

                if [ -d "$DESTDUMPTEMPDIR/$slice/math" ] ; then
                    echo "Compressing math images..."
                    mkdir "$DESTDUMPTEMPDIR/$slice/math_converted"
                    mv -- "$DESTDUMPTEMPDIR/$slice/math/*.err" "$DESTDUMPTEMPDIR/$slice/math_converted"
                    find "$DESTDUMPTEMPDIR/$slice/math" -type f -print0 | \
                    nice xargs -0 pngcrush -q -rem alla -d "$DESTDUMPTEMPDIR/$slice/math_converted" || (sendlog; exit 1)
                    rm -r "$DESTDUMPTEMPDIR/$slice/math"
                fi

                echo "Compressing files..."
                nice tar -czf "$DESTDUMPDIR/slice_$slice.tar.gz" -C "$DESTDUMPTEMPDIR/$slice" "." || (sendlog; exit 1)
                rm -r "$DESTDUMPTEMPDIR/$slice"
                size=($(du -k "$DESTDUMPDIR/slice_$slice.tar.gz"))
                size=${size[0]}
                if [ "$size" -lt 100 ]
                then
                    #echo "The archive is rather small, sending log to server just in case."
                    #$CURL -F logfile=@"$REPODIR/log" "$DUMPATHOMESERVER/client_error_occured"
                    echo "The archive is rather small."
                fi
                echo "Uploading files..."
                $CURL --retry 5 -F user_id="$user_id" -F user="$user" -F team="$team" -F slice.tar.gz=@"$DESTDUMPDIR/slice_$slice.tar.gz" "$DUMPATHOMESERVER/dumps/$wikilang/$wikidate/$commonsdate/$slice/upload" || (sendlog_nosleep; exit 1)
                rm "$DESTDUMPDIR/slice_$slice.tar.gz" 2>/dev/null
                ) &
                index=$((index+1))
            done
            wait
            rm -r "$DESTDUMPDIR" 2>/dev/null
            mkdir "$DESTDUMPDIR" 2>/dev/null
            echo "Job done."
            ;;
        package)
            slices_dir=${jobdesc[1]}
            date=${jobdesc[2]}
            language=${jobdesc[3]}
            orig_url=${jobdesc[4]}
            archive_name=${jobdesc[5]}
            target_dir=${jobdesc[6]}
            norm=${jobdesc[7]}
            dateshort=${jobdesc[8]}

            (
            mkdir "$PACKAGEDIR" 2>/dev/null
            mkdir "$PACKAGEDIR"/"$archive_name" 2>/dev/null
            cd "$PACKAGEDIR"/"$archive_name" || (sendlog; continue)
            python "$SCRIPTDIR"/datafile_storage.py --convert "$slices_dir" "$date" "$language" "$orig_url" "$norm" || (sendlog; continue)
            #rm title_list.txt
            #cd /tmp/evopedia_dumps
            #zip -r -1 -m "$target_dir"/"$archive_name".zip "$archive_name" || (sendlog; continue)
            $CURL --retry 20 -F user_id="$user_id" -F user="$user" -F team="$team" "$DUMPATHOMESERVER/dumps/$language/$dateshort/packaged" || (sendlog; exit 1)
            )
            ;;
        update)
            # allow tar to overwrite the currently executing script
            cp "$SCRIPTDIR"/dumpathome.sh "$SCRITDIR"/dumpathome.sh_old 2>/dev/null
            rm "$SCRIPTDIR"/dumpathome.sh
            mv "$SCRIPTDIR"/dumpathome.sh_old "$SCRITDIR"/dumpathome.sh 2>/dev/null
            $CURL -L -f "$DUMPATHOMESERVER/client" | tar -C "$REPODIR" -xz || (sendlog; continue)
            exec "$SCRIPTDIR"/dumpathome.sh
            exit 0
            ;;
        cleanimporttemp)
            rm -r -- "$IMPORTTEMPDIR/*"
            ;;
        cleantmp)
            find /tmp -name timeline\* -type f -print0 | xargs -0 /bin/rm
            find /tmp -name EasyTimeline\* -type f -print0 | xargs -0 /bin/rm
            ;;
        wait)
            echo "Sleeping 2 hours..."
            sleep 200
            ;;
        *)
            echo "Error: Unknown job."
            sendlog
            echo "Sleeping 2 hours..."
            sleep 200
            ;;
    esac
    ) 2>&1 | tee "$logfile"
    if [ -n "$job_cmdline_override" ]
    then
	echo "Manually requested single job done. Exiting."
	exit
    fi
done
